# Artist4All

### Description

This project is the final project I created on my higher vocational training as a Web developer. It's a social media website made for artists. You can post publications, follow people, write comments on publications and it has a notification service!

### Images

Home page

![Home page](/readme_images/home.png)

Profile page

![Profile page](/readme_images/profile.png)

Configuration page

![Configuration page](/readme_images/configuration.png)

Comments page

![Comments page](/readme_images/configuration.png)

### Installation

- Download and install Angular

    `npm install -g @angular/cli`

- Download and install Docker

- Run composer.sh

- Run the docker image

    `docker-compose up -d`

- Create the database and import the PhpMyAdmin export on artist4all_angular/src/database

- On artist4all_angular folder start the angular project

    `ng serve -o`
